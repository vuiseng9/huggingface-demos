# how to run this script
# source env.sh

import time

import torch
import intel_extension_for_pytorch
from diffusers import StableDiffusionPipeline
from diffusers import DPMSolverMultistepScheduler

prompt = "sailing ship in storm by Rembrandt"
model_id = "runwayml/stable-diffusion-v1-5"
nb_pass = 10

# toggle bool to change datatype
if True:
    dtype=torch.float16
else:
    dtype=torch.float32

dpm = DPMSolverMultistepScheduler.from_pretrained(model_id, subfolder="scheduler")
pipe = StableDiffusionPipeline.from_pretrained(model_id, scheduler=dpm, torch_dtype=dtype).to("xpu")

actual_dtype = next(pipe.unet.parameters()).dtype
actual_device = next(pipe.unet.parameters()).device

def elapsed_time(pipeline, nb_pass=10, num_inference_steps=20):
    start = time.time()
    for _ in range(nb_pass):
        _ = pipeline(prompt, num_inference_steps=num_inference_steps, output_type="np")
    end = time.time()
    return (end - start) / nb_pass


# warmup
images = pipe(prompt, num_inference_steps=10).images

time_original_model = elapsed_time(pipe, nb_pass=nb_pass)

print(f"{time_original_model:.5f} | {actual_device} | {actual_dtype}")
