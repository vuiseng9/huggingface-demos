#Setup
```bash
# nvidia gpu -----------
pip install transformers diffusers accelerate torch==1.13.1

# --------------------------------------------------------------------------------------
# intel gpu
pip install transformers diffusers accelerate

wget https://intel-optimized-pytorch.s3.cn-north-1.amazonaws.com.cn/ipex_stable/xpu/intel_extension_for_pytorch-1.13.10%2Bxpu-cp38-cp38-linux_x86_64.whl
wget https://intel-optimized-pytorch.s3.cn-north-1.amazonaws.com.cn/torch_ccl/xpu/oneccl_bind_pt-1.13.100%2Bgpu-cp38-cp38-linux_x86_64.whl
wget https://intel-optimized-pytorch.s3.cn-north-1.amazonaws.com.cn/ipex_stable/xpu/torch-1.13.0a0%2Bgitb1dde16-cp38-cp38-linux_x86_64.whl
wget https://intel-optimized-pytorch.s3.cn-north-1.amazonaws.com.cn/ipex_stable/xpu/torchvision-0.14.1a0%2B0504df5-cp38-cp38-linux_x86_64.whl

pip install torch-1.13.0a0+gitb1dde16-cp38-cp38-linux_x86_64.whl torchvision-0.14.1a0+0504df5-cp38-cp38-linux_x86_64.whl

# Running IPEX on GPU requires oneapi basekit (dpc++ compiler, mkl)
# * install using apt has challenge to connect to server apt.repo.intel.com
# * conda -c intel ... ... some shared libs are missing
# * just download from oneapi page https://www.intel.com/content/www/us/en/docs/oneapi/installation-guide-linux/2023-0/conda.html
sh ./l_BaseKit_p_2023.0.0.25537_offline.sh

# modify env.sh accordingly
# run python sd_intel_xpu.py

# monitor intel gpui #note should move to XPU Manager https://github.com/intel/xpumanager
sudo apt-get install intel-gpu-tools 
sudo intel_gpu_top
```

# benchmark

```
# julien-sd-rtx
python sd_cuda.py

# julien-sd-spr
path/to/julien-sd-spr/bin/python3 sd_blog_3.py 

# julien-sd-a770
source env.sh
python sd_intel_xpu.py
```