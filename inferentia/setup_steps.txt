inf1.6xlarge, 256GB storage
Deep Learning AMI PyTorch, Amazon Linux 2 (ami-04869024f1c986686)

# Adapted from https://awsdocs-neuron.readthedocs-hosted.com/en/latest/frameworks/torch/torch-neuron/setup/pytorch-install.html

sudo tee /etc/yum.repos.d/neuron.repo > /dev/null <<EOF
[neuron]
name=Neuron YUM Repository
baseurl=https://yum.repos.neuron.amazonaws.com
enabled=1
metadata_expire=0
EOF

sudo rpm --import https://yum.repos.neuron.amazonaws.com/GPG-PUB-KEY-AMAZON-AWS-NEURON.PUB
sudo yum update -y
sudo yum install kernel-devel-$(uname -r) kernel-headers-$(uname -r) -y
sudo yum install aws-neuronx-dkms -y
sudo yum install aws-neuron-tools -y

export PATH=/opt/aws/neuron/bin:$PATH

sudo yum install -y python-virtualenv gcc-c++
virtualenv pytorch_venv
source pytorch_venv/bin/activate

pip3 config set global.extra-index-url https://pip.repos.neuron.amazonaws.com
pip3 install torch-neuron neuron-cc[tensorflow] "protobuf==3.20.1"

# see https://github.com/aws-neuron/aws-neuron-sdk/issues/474
pip3 install "transformers<4.20.0" 
pip3 install joblib

export TOKENIZERS_PARALLELISM=false

# Grab model from S3 (you won't be allowed to do this)
# Watch https://youtu.be/HweP7OYNiIA to see how I trained this model with Trainium
aws s3 cp s3://jsimon-test-useast1/model.pt .
